
lazy val scalaTestVersion = "3.0.5"

lazy val `sbt-gitlabpublisher` = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "sparetimecoders",
      scalaVersion := "2.12.6"
    )),
    name := "sbt-gitlabpublisher",
    publishTo := Some("Gitlab Repository Manager" at "https://gitlab.com/api/v4/projects/11897413/packages/maven"),
    releaseIgnoreUntrackedFiles := true,
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % scalaTestVersion % Test
    )
  ).enablePlugins(SbtPlugin)


enablePlugins(GitlabPublisherPlugin)


