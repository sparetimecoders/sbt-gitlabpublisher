package com.sparetimecoders

import okhttp3.{MediaType, OkHttpClient, Request, RequestBody}
import org.apache.ivy.util.{CopyProgressEvent, CopyProgressListener}
import sbt.Keys.onLoad
import sbt.internal.librarymanagement.ivyint.GigahorseUrlHandler
import sbt.{Def, File, Global, URL}

object GitlabPublisher extends GigahorseUrlHandler with EnvVariables {

  private lazy val okHttpClient: OkHttpClient = {
    GigahorseUrlHandler.http
      .underlying[OkHttpClient]
      .newBuilder()
      .followRedirects(true)
      .followSslRedirects(true)
      .build
  }

  private val EmptyBuffer: Array[Byte] = new Array[Byte](0)

  override def upload(source: File, dest0: URL, l: CopyProgressListener): Unit = {
    val dest = normalizeToURL(dest0)

    val body = RequestBody.create(MediaType.parse("application/octet-stream"), source)
    val request = new Request.Builder()
      .url(dest)
      .addHeader(gitlabHeader().name, gitlabHeader().value)
      .put(body)
      .build()

    if (l != null) {
      l.start(new CopyProgressEvent())
    }
    val response = okHttpClient.newCall(request).execute()
    try {
      if (l != null) {
        l.end(new CopyProgressEvent(EmptyBuffer, source.length()))
      }
      validatePutStatusCode(dest, response.code(), response.message())
    } finally {
      response.close()
    }
  }
}

object GitlabPublisherPlugin extends sbt.AutoPlugin with EnvVariables {

  override def projectSettings: Seq[Def.Setting[_]] = {
    Seq(
      onLoad in Global := (onLoad in Global).value andThen { state =>
        import org.apache.ivy.util.url._

        val urlHandlerDispatcher = new URLHandlerDispatcher {
          super.setDownloader("http", GitlabPublisher)
          super.setDownloader("https", GitlabPublisher)

          override def setDownloader(protocol: String, downloader: URLHandler): Unit = {}
        }
        URLHandlerRegistry.setDefault(urlHandlerDispatcher)
        state
      }
    )
  }
}

trait EnvVariables {

  case class GitlabHeader(name: String, value: String)

  def gitlabHeader(): GitlabHeader = {
    sys.env.get("CI_JOB_TOKEN")
      .map(s => GitlabHeader("Job-Token", s))
      .orElse(sys.env.get("GITLAB_TOKEN")
        .map(s => GitlabHeader("Private-Token", s)))
      .orElse(throw new IllegalArgumentException("Missing gitlab token from environment, please provide either CI_JOB_TOKEN or GITLAB_TOKEN"))
      .get
  }
}
